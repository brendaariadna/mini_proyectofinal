
import CalcyApp.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.CORBA.*;
import java.util.Scanner;
import java.lang.*;

public class CalcyClient
{
  static CalcyInterface CalcyImpl;
  static int flag=1;
  static double x=0.0d;
  static double y=0.0d;
  static int opc;
  
  public static void main(String args[])
   {
        
    try{
                 System.out.println("\n----------------------------------------------");
System.out.println("Conectado con el Servidor");
           
                // create and initialize the ORB
                ORB orb = ORB.init(args, null);

                // get the root naming context
                org.omg.CORBA.Object objRef = 
                orb.resolve_initial_references("NameService");

                // Use NamingContextExt instead of NamingContext. 
                // This is part of the Interoperable naming Service.  
                NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

                // resolve the Object Reference in Naming
                String name = "CalcyOperations";
                CalcyImpl = CalcyInterfaceHelper.narrow(ncRef.resolve_str(name));

                  System.out.print("Obteniendo Conexion \n\n");
                  System.out.println(CalcyImpl);

                Scanner sc=new Scanner(System.in);
      
                flag=1;

                do
                {
                        System.out.println("\n***Seleccione una Opcion***\t");
                        System.out.println("--------------------------------------------------");
                        System.out.println("\n1)Suma:\t");
                        System.out.println("\n2)Resta:\t");
                        System.out.println("\n3)Multiplicacion:\t");
                        System.out.println("\n4)Division:\t");
                        System.out.println("\n5)Potencia:\t");
                        opc=sc.nextInt();

                System.out.println("--------------------------------------------------");
                System.out.print("\nEscriba el primer numero: ");
                 x=sc.nextDouble();

                System.out.print("\nEscriba el segundo Numero: ");
                y=sc.nextDouble();

                  if(opc==1)
                  {
                    System.out.println("\nResultado Suma:\t= "+CalcyImpl.add(x,y));
                  }
                  else 
                    if(opc==2)
                    {
                      System.out.println("\nResultado Resta:\t= "+CalcyImpl.subtract(x,y));
                    }
                  else
                    if(opc==3)
                      {
                        System.out.println("\nResultado Multiplicacion:\t= "+CalcyImpl.multiply(x,y));
                       }
                   else 
                    if(opc==4){
                      System.out.println("\nResultado Division\t= "+CalcyImpl.divide(x,y));
                    }
                    else
                    	if(opc==5) {
                    		System.out.println("\n Resultado Potencia\t="+CalcyImpl.pow2(x,y));
                    	}
                   
                System.out.println("--------------------------------------------------");
                System.out.println("Desea Continuar?[1:Si|0:No]: ");
                flag=sc.nextInt();
                }
                while (flag!=0);

                CalcyImpl.shutdown();
         }
            catch (Exception e) 
            {   
                 
                 System.out.println("\n----------------------------------------------");
System.out.println("Error en el Cliente !");
                 System.out.println("\n----------------------------------------------");
            }

System.out.println("\nCerrando...");
System.out.println("\n----------------------------------------------");
        }
}