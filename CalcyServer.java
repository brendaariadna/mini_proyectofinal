
    import CalcyApp.*;
    import org.omg.CosNaming.*;
    import org.omg.CosNaming.NamingContextPackage.*;
    import org.omg.CORBA.*;
    import org.omg.PortableServer.*;
    import org.omg.PortableServer.POA;
    import java.util.Properties;

class CalcyInterfaceImpl extends CalcyInterfacePOA  
{
    private ORB orb;

    public void setORB(ORB orb_val) 
    {   
        orb = orb_val;
    }


    public String add(double x, double y)
    {
        System.out.println(x+" + "+y);
        double res= x+y;
        System.out.println("Res="+res);
        return Double.toString(res);
    }

    public String subtract(double x, double y)
    {
        System.out.println(x+" - "+y);
        double res= x-y;
        System.out.println("Res="+res);
        return Double.toString(res);
    }

    public String multiply(double x, double y)
    {
        System.out.println(x+" * "+y);
        double res= x*y;
        System.out.println("Res="+res);
        return Double.toString(res);
    }

    public String divide(double x, double y)
    {
        System.out.println(x+" / "+y);
        double res= x/y;
        System.out.println("Res="+res);
        return Double.toString(res);
    }
    
    public String pow2(double x, double y)
    {
        System.out.println(x+" ^ "+y);
        double res= Math.pow(x, y);
        System.out.println("Res="+res);
        return Double.toString(res);
    }
    


    public void shutdown()
    {
            orb.shutdown(false);
    }
}

public class CalcyServer
{
    public static void main(String args[]) 
   {

    try{
         // Create and initialize the ORB  
         ORB orb = ORB.init(args, null);  
        

        POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
        rootpoa.the_POAManager().activate();


        CalcyInterfaceImpl CalcyImpl = new CalcyInterfaceImpl();
        CalcyImpl.setORB(orb);

    
        org.omg.CORBA.Object ref = rootpoa.servant_to_reference(CalcyImpl);
        CalcyInterface href = CalcyInterfaceHelper.narrow(ref);

      org.omg.CORBA.Object objRef =orb.resolve_initial_references("NameService");

        // Use NamingContextExt which is part of the Interoperable
        // Naming Service (INS) specification.
        NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

      String name = "CalcyOperations";
        NameComponent path[] = ncRef.to_name( name );
        ncRef.rebind(path, href);
       System.out.println("\n----------------------------------------------");
       System.out.println("Server Waiting....");
       System.out.println("\n----------------------------------------------");
    
      // System.out.println(res);
       //System.out.println(orb);

  
        orb.run();

        }
            catch (Exception e) 
            {   

       System.out.println("\n----------------------------------------------");
       System.out.println("Errror en el Servidor !");
       System.out.println("\n----------------------------------------------");
            }
      System.out.println("\nCerrado...");
     System.out.println("\n----------------------------------------------");
        }
    }
 