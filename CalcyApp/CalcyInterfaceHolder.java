package CalcyApp;

/**
* CalcyApp/CalcyInterfaceHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from CalcyInterface.idl
* miercoles 10 de junio de 2020 02:29:24 AM CDT
*/

public final class CalcyInterfaceHolder implements org.omg.CORBA.portable.Streamable
{
  public CalcyApp.CalcyInterface value = null;

  public CalcyInterfaceHolder ()
  {
  }

  public CalcyInterfaceHolder (CalcyApp.CalcyInterface initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = CalcyApp.CalcyInterfaceHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    CalcyApp.CalcyInterfaceHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return CalcyApp.CalcyInterfaceHelper.type ();
  }

}
